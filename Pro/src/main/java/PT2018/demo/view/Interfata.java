package PT2018.demo.view;

import java.awt.*;
import javax.swing.*;

public class Interfata  {
	JTextField poll1=new JTextField("                                                                    ");
	JTextField poll2=new JTextField("                                                                    ");
	JButton adunare=new JButton("Adunare");
	JButton scadere=new JButton("Scadere");
	JButton inmultire=new JButton("Inmultire");
	JButton impartire=new JButton("Impartire");
	JButton derivare=new JButton("Derivare");
	JButton integrare=new JButton("Integrare");
	JLabel pol1=new JLabel("Introduceti primul polinom:");
	JLabel pol2=new JLabel("Introduceti al doilea polinom:");
	JButton ok=new JButton("ok");
	JLabel rez1=new JLabel(" Rezultatul este:  ");
	
	public Interfata()
	{
		JFrame f=new JFrame();
	JPanel p1=new JPanel();
	p1.setLayout(new FlowLayout());
	p1.add(pol1);
	p1.add(poll1);
	JPanel p2=new JPanel();
	p2.setLayout(new FlowLayout());
	p2.add(pol2);
	p2.add(poll2);
	
	

	
	JPanel p4=new JPanel();
	p4.setLayout(new BoxLayout(p4,BoxLayout.X_AXIS));
	p4.add(Box.createRigidArea(new Dimension(0,50)));
	p4.add(adunare);
	p4.add(scadere);
	p4.add(inmultire);
	
	JPanel p5=new JPanel();
	p5.setLayout(new BoxLayout(p5,BoxLayout.X_AXIS));
	p5.add(impartire);
	p5.add(derivare);
	p5.add(integrare);
	
	JPanel p6=new JPanel();
	p6.setLayout(new BoxLayout(p6,BoxLayout.Y_AXIS));
	p6.add(p1);
	p6.add(p2);
	
	p6.add(ok);
	p6.add(p4);
	p6.add(p5);
	p6.add(rez1);
	
	p6.add(Box.createRigidArea(new Dimension(0,20)));
	
	
f.setContentPane(p6);
	
	f.setDefaultCloseOperation(f.EXIT_ON_CLOSE);
	f.setSize(new Dimension(400,300));
	f.setLocation(300,300);
	f.setVisible(true);
	}
	
	
	public String getPol1()
	{return this.poll1.getText();}
	
	public String getPol2()
	{return this.poll2.getText();}
	
	public void setRez1(String v)
	{this.rez1.setText(v);}
	

	
	
	
	
    
}
